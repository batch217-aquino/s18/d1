// function printInput(){
//     let nickname = prompt("Nickname: ")
//     console.log("Hi, "+ nickname)
// }
// printInput();

function printName(name){
    console.log("My name is "+ name);
}

printName("Jose Lawrence Aquino");

let sampleVar = "Yui";
printName(sampleVar);

function checkDivisibilityBy8(num){
    let remainder = num % 8;
    console.log("The remainder of "+ num + "divided by 8 is: "+ remainder);

    let isDivisibleBy8 = remainder === 0;
    console.log("Is "+ num + " divisible by 8?");
    console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(32);

// Functions as Argument

function argumentFunction(){
    console.log("This function was passed as an before the msg.")
}
function invokeFunction(argumentFunction){
    argumentFunction();
}
invokeFunction(argumentFunction)

console.log(argumentFunction);

// Multiple parameters

function createFullName(firstName, middleName, lastName){
    console.log(firstName + " " + middleName + " " + lastName)

}
createFullName("Jose Lawrence", "Webon", "Aquino")
createFullName("Jose Lawrence", "Webon",)

// Multiple parameter using stored data in a variable
let firstName ="John";
let middleName = 'Webon';
let lastName = 'Aquino';
createFullName(firstName, middleName, lastName);

function printFullName(middleName, firstName, lastName){
    console.log(firstName+ ' ' + middleName + ' ' + lastName);
}
printFullName('Juan', 'Dela', 'Cruz');


// Return Statement
function returnFullName(firstName, middleName, lastName){
    
    
    
    
    return firstName + ' ' + middleName + ' ' + lastName
    console.log("This is not be listed in the console.")
}

let completeName = returnFullName('Juan', 'Dela', 'Cruz')
console.log(completeName)



console.log(returnFullName(firstName, middleName, lastName))

function returnAddress(city, country){
    let fullAddress = city + ", " + country;
    return fullAddress;
    console.log("This is not be displayed");
}
let myAddress = returnAddress("Cebu City", "Philippines");
console.log(myAddress)

function printPlayerInfo(username, level, job){
    // console.log("Username: "+ username);
    // console.log("Level: "+ level);
    // console.log("Job: "+ job);
    return "Username" + username;

}
let user1 = printPlayerInfo("Knight_white", 95, "Paladin")
console.log(user1)